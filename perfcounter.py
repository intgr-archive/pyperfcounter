"""A ctypes-based Python wrapper of Linux's new performance counter subsystem

Currently you have to patch your kernel to get perfcounter support:
http://people.redhat.com/mingo/perfcounters/

This wrapper is compatible with version 8 of the patch.

Until glibc gets support for the perf_counter_open() syscall, this library uses
the glibc syscall(2) interface.

Simple usage::

  import perfcounter, time
  ctr = perfcounter.perfcounter('cpu-cycles', cpu=0)

  lastval = 0
  while True:
    time.sleep(1)
    val = ctr.get()
    print val - lastval
    lastval = val

"""

import ctypes
import struct
import os
import platform
import fcntl

# enum perf_event_types
PERF_TYPE_HARDWARE              = 0
PERF_TYPE_SOFTWARE              = 1
PERF_TYPE_TRACEPOINT            = 2
PERF_TYPE_HW_CACHE              = 3
PERF_TYPE_RAW                   = 128

# enum hw_event_ids
PERF_COUNT_CPU_CYCLES           = 0
PERF_COUNT_INSTRUCTIONS         = 1
PERF_COUNT_CACHE_REFERENCES     = 2
PERF_COUNT_CACHE_MISSES         = 3
PERF_COUNT_BRANCH_INSTRUCTIONS  = 4
PERF_COUNT_BRANCH_MISSES        = 5
PERF_COUNT_BUS_CYCLES           = 6

# enum sw_event_ids
PERF_COUNT_CPU_CLOCK            = 0
PERF_COUNT_TASK_CLOCK           = 1
PERF_COUNT_PAGE_FAULTS          = 2
PERF_COUNT_CONTEXT_SWITCHES     = 3
PERF_COUNT_CPU_MIGRATIONS       = 4
PERF_COUNT_PAGE_FAULTS_MIN      = 5
PERF_COUNT_PAGE_FAULTS_MAJ      = 6

# ioctl requests
PERF_COUNTER_IOC_ENABLE         = 0x2400
PERF_COUNTER_IOC_DISABLE        = 0x2401
PERF_COUNTER_IOC_REFRESH        = 0x2402
PERF_COUNTER_IOC_RESET          = 0x2403

# enum perf_counter_read_format - these flags can be combined
PERF_FORMAT_TOTAL_TIME_ENABLED  = 1
PERF_FORMAT_TOTAL_TIME_RUNNING  = 2
PERF_FORMAT_ID                  = 4

# taken directly from linux/tools/pref/util/parse-events.c using the regexp:
# s/{ C(\([A-Z_]\+\), \([A-Z_]\+\)),\s*\([a-z"-]\+\),\(\s*\)},/\3:\4 (PERF_TYPE_\1, PERF_COUNT_\2),/

event_symbols = {
    "cpu-cycles":            (PERF_TYPE_HARDWARE, PERF_COUNT_CPU_CYCLES),
    "cycles":                (PERF_TYPE_HARDWARE, PERF_COUNT_CPU_CYCLES),
    "instructions":          (PERF_TYPE_HARDWARE, PERF_COUNT_INSTRUCTIONS),
    "cache-references":      (PERF_TYPE_HARDWARE, PERF_COUNT_CACHE_REFERENCES),
    "cache-misses":          (PERF_TYPE_HARDWARE, PERF_COUNT_CACHE_MISSES),
    "branch-instructions":   (PERF_TYPE_HARDWARE, PERF_COUNT_BRANCH_INSTRUCTIONS),
    "branches":              (PERF_TYPE_HARDWARE, PERF_COUNT_BRANCH_INSTRUCTIONS),
    "branch-misses":         (PERF_TYPE_HARDWARE, PERF_COUNT_BRANCH_MISSES),
    "bus-cycles":            (PERF_TYPE_HARDWARE, PERF_COUNT_BUS_CYCLES),

    "cpu-clock":             (PERF_TYPE_SOFTWARE, PERF_COUNT_CPU_CLOCK),
    "task-clock":            (PERF_TYPE_SOFTWARE, PERF_COUNT_TASK_CLOCK),
    "page-faults":           (PERF_TYPE_SOFTWARE, PERF_COUNT_PAGE_FAULTS),
    "faults":                (PERF_TYPE_SOFTWARE, PERF_COUNT_PAGE_FAULTS),
    "minor-faults":          (PERF_TYPE_SOFTWARE, PERF_COUNT_PAGE_FAULTS_MIN),
    "major-faults":          (PERF_TYPE_SOFTWARE, PERF_COUNT_PAGE_FAULTS_MAJ),
    "context-switches":      (PERF_TYPE_SOFTWARE, PERF_COUNT_CONTEXT_SWITCHES),
    "cs":                    (PERF_TYPE_SOFTWARE, PERF_COUNT_CONTEXT_SWITCHES),
    "cpu-migrations":        (PERF_TYPE_SOFTWARE, PERF_COUNT_CPU_MIGRATIONS),
    "migrations":            (PERF_TYPE_SOFTWARE, PERF_COUNT_CPU_MIGRATIONS),
    }


#### LOW-LEVEL WRAPPERS

try:
  libc = ctypes.CDLL("libc.so.6", use_errno=True)
except TypeError:
  # Python 2.5 and earlier don't have use_errno
  libc = ctypes.CDLL("libc.so.6")

# ugly detection of the 'perf_counter_open' syscall number
if platform.machine() in ['x86_64', 'i386', 'i686']:
  # determine bitness of running _interpreter_, not kernel
  if platform.architecture()[0] == '64bit':
    SYSCALL_perf_counter_open = 298 # x86_64
  else:
    SYSCALL_perf_counter_open = 336 # x86
elif platform.machine() == 'ppc':
  SYSCALL_perf_counter_open = 319 # ppc
else:
  raise ImportError, "Your platform is not supported"

class perf_counter_hw_event(ctypes.Structure):
  """ctypes structure from linux/include/linux/perf_counter.h"""
  _fields_ = [
      ("type", ctypes.c_uint32),        # see enum perf_event_types
      ("__reserved_1", ctypes.c_uint32),
      ("config", ctypes.c_uint64),      # see attr_ids and sw_event_ids
      ("sample_freq", ctypes.c_uint64),
      ("sample_type", ctypes.c_uint64),
      ("read_format", ctypes.c_uint64),
      ("flags", ctypes.c_uint64),       # given as bit members in C struct
      ("extra_config_len", ctypes.c_uint32),
      ("wakeup_events", ctypes.c_uint32),
      ("__reserved_3", ctypes.c_uint32),
      ("__reserved_4", ctypes.c_uint64)]

def check_errno(ret):
  """Raises OSError with errno if ret is less than 0"""
  if ret < 0:
    if hasattr(ctypes, 'get_errno'):
      errno = ctypes.get_errno()
    else:
      errno = ctypes.c_int.in_dll(libc, 'errno').value
    raise OSError(errno, os.strerror(errno))

# int syscall(int number, ...);
# int perf_counter_open(struct perf_counter_hw_event *hw_event_uptr,
#                       pid_t pid, int cpu, int group_fd,
#                       unsigned long flags);

def perf_counter_open(hw_event, pid, cpu, group_fd, flags):
  """A straightforward ctypes wrapper of perf_counter_open()"""
  assert isinstance(hw_event, perf_counter_hw_event)
  ret = libc.syscall(SYSCALL_perf_counter_open, ctypes.byref(hw_event),
      pid, cpu, group_fd, ctypes.c_ulong(flags))
  check_errno(ret)
  return ret

def count_set_bits(val):
  """Count set bits in an integer"""
  cnt = 0
  while val > 0:
    val &= val - 1
    cnt += 1
  return cnt

#### HIGH-LEVEL INTERFACE

def event_from_symbol(name):
  """Factory for creating perf_counter_hw_event from a symbolic event name"""

  if name not in event_symbols:
    raise ValueError, "Unknown event symbol: %s" % name
  type, config = event_symbols[name]

  return perf_counter_hw_event(type=type, config=config)

class perfcounter(object):
  """Provides the "obvious" interface to perfcounters

  perfcounter(event, pid=-1, cpu=-1, group=None)
    event: symbolic counter name (see event_symbols) or perf_counter_hw_event object
    pid: follow the given process ID
    cpu: follow the global counters on a given CPU
    group: parent perfcounter instance whose commands also affect this counter

  Basic API:
    get() - read and return the current value of the counter
    close() - close the counter
  """

  def __init__(self, event, pid=-1, cpu=-1, group=None):
    assert pid >= 0 or cpu >= 0, "Either 'pid' or 'cpu' must be specified"

    if isinstance(event, str):
      self.event = event_from_symbol(event)
    else:
      self.event = event

    if group:
      group_fd = group.fileno()
    else:
      group_fd = -1

    self.fd = perf_counter_open(self.event, pid, cpu, group_fd, 0)

    # for each bit in read_format, the kernel gives an additional 64-bit value
    self.value_count = 1 + count_set_bits(self.event.read_format)

  def __del__(self):
    self.close()

  def close(self):
    """Close the file descriptor of this counter"""

    if hasattr(self, 'fd'):
      os.close(self.fd)
      del self.fd

  def fileno(self):
    """Returns the file descriptor.

    This means that you can add perfcounter objects directly into
    select()/poll() lists without extracting the file descriptor manually."""

    return self.fd

  def read(self, len):
    """Reads 'len' bytes from the counter object and returns a string"""

    return os.read(self.fd, len)

  def get(self):
    """Returns current counter value as a long integer"""

    vals = struct.unpack(self.value_count * 'Q', self.read(self.value_count * 8))
    if self.value_count == 1:
      return vals[0]
    return vals

  def enable(self):
    fcntl.ioctl(self.fd, PERF_COUNTER_IOC_ENABLE)

  def disable(self):
    fcntl.ioctl(self.fd, PERF_COUNTER_IOC_DISABLE)

  def refresh(self, events):
    fcntl.ioctl(self.fd, PERF_COUNTER_IOC_REFRESH, events)

  def reset(self):
    fcntl.ioctl(self.fd, PERF_COUNTER_IOC_RESET)


