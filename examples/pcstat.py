#!/usr/bin/env python
"""Reports global performance counter statistics, like vmstat/iostat/etc

Currently only reports information about the first CPU
"""

import sys
import os
import time

# hack to make import work from the examples directory
dir = os.path.dirname(os.path.abspath(__file__))
if dir.endswith('/examples'):
  sys.path.append(os.path.dirname(dir))

import perfcounter

def format(counter):
  """Formats the number in a 4-character format like '1.1M' or '100k'"""

  suffixes = ' kMGTP'
  
  i = 0
  while counter >= 1000:
    counter = (counter) / 1000.0
    i += 1

  if counter <= 9.95 and i > 0:
    return '%1.1f%s' % (counter, suffixes[i])
  else:
    return '%3d%s' % (counter, suffixes[i])

def main():
  if len(sys.argv) > 1:
    events = sys.argv[1:]
  else:
    events = ['cycles', 'instructions', 'cache-references', 'cache-misses',
        'branches', 'branch-misses', 'faults', 'major-faults', 'minor-faults',
        'cs', 'migrations']

  counters = []
  for event in events:
    try:
      counter = perfcounter.perfcounter(event, cpu=0)
      counter.name = event

      counters.append(counter)
    except Exception, ex:
      print>>sys.stderr, 'Cannot open %s: %s' % (event, ex)

  if not counters:
    print>>sys.stderr, 'No counters to report'
    print 'Recognized events:', ' '.join(perfcounter.event_symbols.keys())
    sys.exit(1)

  # print column headers
  print ' '.join(ctr.name[:5].center(5) for ctr in counters)

  oldvalues = [0] * len(counters)
  while True:
    time.sleep(1)
    values = [ctr.get() for ctr in counters]
    deltas = [cur - old for cur, old in zip(values, oldvalues)]
    print ' ' + '  '.join(map(format, deltas))
    oldvalues = values

if __name__ == '__main__':
  main()

